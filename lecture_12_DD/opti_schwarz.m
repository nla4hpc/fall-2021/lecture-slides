function [errorvet, conv_iter, th_err]=opti_schwarz(a,d,maxiter,tol, opti_param, eta, J,h)

%=============================================
% This code has been taken from the Advanced Domain Decomposition Summer
% school held at P. Milano, Nov 2021
% and has been modified for our use
%=============================================
                                                          
room_data;
iters=(1:maxiter+1)';
f1=f(:,1:a+d+1);f2=f(:,a+1:end);                                           % define local force terms
u1=zeros(J,a+d+1); u2=zeros(J,J-a+2);                                      % zero initial guess
x1=(0:h:(a+d)*h);x2=(a*h:h:1); y=(0:h:1);                                  % finite difference meshes
z1=zeros(1,a+d+1);z2=zeros(1,J-a+2);z3=zeros(1,J+2);                       % for plotting purposes
p=opti_param;                                                                      
e=ones(J,1);                                                               % support vector to construct normal derivatives
pe=1e12;                                                                   % large Robin parameter to emulate a Dirichlet condition by penalty
A=A2d(eta,h,J+2,J);                                                        % defined global stiffness matrix
u=Solve2dR(A,f,h,J+2,J,pe*gg,pe*gd,pe,pe);                                 % solve global problem
z1=zeros(1,a+d+1);z2=zeros(1,J-a+2); z3=zeros(1,J+2);                      % for plotting purposes
u=[z3;u;z3];                                                               % Add horizontal homogeneous Dirichlet Bc.
Nx1=a+d+1;  Nx2=J+2-a;                                                     % define sizes of subdomains.
Ny1=J; Ny2=J;
A1=A2d(eta,h,Nx1,Ny1);                                                     % define subdomain matrices
A2=A2d(eta,h,Nx2,Ny2);
errorvet(1)=norm(u,2);                                                  
Na=[sparse(eye(J,J)),-sparse(diag(-e(1:end-1)/2,-1)+diag((eta*h^2+4)*e/2)+diag(-e(1:end-1)/2,1))]/h; %operators which extract Neumann data
Nb=[-sparse(diag(-e(1:end-1)/2,-1)+diag((eta*h^2+4)*e/2)+diag(-e(1:end-1)/2,1)),sparse(eye(J,J)) ]/h;


for i=1:maxiter
    tb=Nb*[u2(:,d+1);u2(:,d+2)]+f2(:,d+1)*h/2+p*u2(:,d+1);                 % derive Robin boundary data from old approximations
    ta=Na*[u1(:,end-d-1);u1(:,end-d)]+f1(:,end-d)*h/2+p*u1(:,end-d);
    u1n=Solve2dR(A1,f1,h,Nx1,Ny1,pe*gg,tb,pe,p);                           % Solve subdomain problems
    u2n=Solve2dR(A2,f2,h,Nx2,Ny2,ta,pe*gd,p,pe);                
    u1=u1n;
    u2=u2n;
    ufin=[u1n(:,1:a),(u1n(:,a+1:a+d+1)+u2n(:,1:d+1))/2,u2n(:,d+2:end)];    % average contribution in the overlap
    errorvet(i+1)=norm(u-[z3;ufin;z3],2);
    if(errorvet(i+1) < tol)
        break;
    end
end
conv_iter=i;
conv_iters=1:conv_iter+1;
 beta=(a+1)*h;                                                              % location of the interfaces
 alpha=(a+d+1)*h;
 k=(1:J)*pi;
 rho=abs(((k.*cosh(k*(1-alpha))-p*sinh(k*(1-alpha)))./(k.*cosh(k*alpha)+p*sinh(k*alpha))).*((k.*cosh(k*beta)-p*sinh(k*beta))./(k.*cosh(k*(1-beta))+p*sinh(k*(1-beta)))));
 th_err = (max(rho).^(conv_iters/2))';

 
end


clear all;
eta=0; %reaction term
J=20;  %number of interior mesh points in x and y direction
h=1/(J+1); %mesh size
overlap = [1 2 4];
left_domain_size = 8;
max_iters=300;
tol=1e-12;
iter=zeros(4, length(overlap));

for ov_lev=1:length(overlap)
    % Non optimized with overlap
    ov=overlap(ov_lev);
    p=1;
    [~,iter(1,ov_lev),~] = opti_schwarz(left_domain_size,ov,max_iters,tol,p,eta,J,h);

    % Non Optimized with no-overlap
    ov=0;
    p=1;
    [~,iter(2,ov_lev),~] = opti_schwarz(left_domain_size,ov,max_iters,tol,p,eta,J,h);
    
    % Optimized with overlap
    ov=overlap(ov_lev);
    p=((pi^2+eta)/(ov*h))^(1/3);
    [~,iter(3,ov_lev),~] = opti_schwarz(left_domain_size,ov,max_iters,tol,p,eta,J,h);
    
    % Optimized with no-overlap
    p=sqrt(pi*pi/h);
    ov=0;
    [~,iter(4,ov_lev),~] = opti_schwarz(left_domain_size,ov,max_iters,tol,p,eta,J,h);
    
end

X = categorical({'non-optimized-overlap' 'non-optimized-no-overlap' 'optimized-overlap' 'optimized-no-overlap'});
X = reordercats(X,{'non-optimized-overlap' 'non-optimized-no-overlap' 'optimized-overlap' 'optimized-no-overlap'});

bar(X,iter)
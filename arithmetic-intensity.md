# CSR Matrix Vector Multiplication
```
for i in [0, rows):
  y[i] = 0
  for idx in [row_ptr[i], row_ptr[i + 1]):
	y[i] += val[idx] * x[col[idx]]
```
This algorithm depicts a naive implementation of multiplying a CSR matrix, defined by `row_ptr`, `col`, and `val`, to a vector `x` and storing the result in `y`.

### Assumptions
The array `row_ptr : [0, rows + 1) -> [0, N)` is injective and monotonly increasing with `row_ptr[0] = 0` and `row_ptr[rows + 1] = N`.
As a consequence, `idx` is always unique for every `i`.
The array `col` has the range `col: [0, N) -> [0, cols)`.
The maximal value of `row_ptr` (the number of non-zero entries of the matrix) is vastly larger than both `rows` and `cols`, i.e. `N >>> rows, cols`.


### Flops
The update `y[i] += ...` is executed `row_ptrs[i + 1] - row_ptrs[i]` times for outer iteration `i`. As the outer iteration is done `rows` times, the total number of iterations for the update are `sum_{i=0}^rows row_ptrs[i + 1] - row_ptrs[i] = row_ptrs[rows + 1] - row_ptrs[0] = N`.
With this the total number of FLOPs is `#FLOP = 2 * N`.

### Memory Access
For each inner `idx` loop the memory access into `y` stays the same and can therefore be cached, leading to `rows` for updating `y[i]`.
Since `idx` is unique for each iteration, both `val[idx]` and `col[idx]` can never be cached. Therefore, accessing both array requires `2 * N` memory accesses (assuming that both types use the same number of bytes).
The `col` mapping can't be injective, so the accesses to `x` could be cached, depending on the pattern given by `col`. In the best case, only `cols` accesses are required and in the worst case, i.e. values of `x` are removed from the cache before they can be reused again, `N` accesses are needed.
Where exactly the accesses to `x` fall, can't be easily predicted, so they are modeled as `alpha * N`, with a parameter `cols/N <= alpha <= 1`.
The total number of memory accesses is now `#MEM = (rows + (2 + alpha) * N) * #Byte`.

### Arithmetic Intensity
The results above lead to the arithmetic intensity:
```
AI = #FLOP/#MEM = 2 * N / ((rows + (2 + alpha) * N) * #Byte) = 2 / ((2 + alpha) * #Byte)
```
using that `rows <<< N`.


# Block-CSR Matrix Vector Multiplication
```
for i in [0, rows):
  for b in [0, bs):
	y[i * bs + b] = 0
  for idx in [row_ptr[i], row_ptr[i + 1]):
	for b_i in [0, bs):
		for b_j in [0, bs):
			y[i * bs + b_i] += val[idx + b_j + b_i * bs] * x[col[idx] + b_j]
```
In contrast to the default CSR format, the values of `val` are now understood as dense `bs x bs` matrix blocks. All other assumptions from above still hold. The arithmetic intensity can be derived very similarily to the one above. How does the AI improve compared to the previous one in the limit wrt. the block size `bs`?

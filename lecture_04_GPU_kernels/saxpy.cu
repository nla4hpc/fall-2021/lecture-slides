#include <stdlib.h>
#include <stdio.h>




// here goes my GPU kernel
__global__ void saxpy_kernel( 
    int n, 
    float alpha, 
    float *dx, 
    float *dy ){
    
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    
    if (idx < n) {
        dy[ idx ] = dy[ idx ] + alpha * dx[ idx ]; 
    }
}





// main program for the CPU

int main() {

    printf("Program start..\n");
    
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);
    
    printf("A float number has %d bytes.\n", sizeof(float));
    printf("A double number has %d bytes.\n", sizeof(double));    
    printf("A int number has %d bytes.\n", sizeof(int));
  
  
    int n = 1000000;
    float *x, *y, *dx, *dy; // define vectors as arrays
    x = (float*) malloc ( n * sizeof(float) ); // allocate memory for x on the CPU
    y = (float*) malloc ( n * sizeof(float) ); // size of the memory needed is n* 32bit (sizeof(float))
    cudaMalloc( (void**) &dx, n * sizeof(float) ); // allocate memory on the GPU for dx
    cudaMalloc( (void**) &dy, n * sizeof(float) ); // allocate memory on the GPU for dy
    for (int i=0; i<n; i++) {
        x[i] = 2.0;   
        y[i] = (double) i;
    }
    
    
    for (int i=0; i<10; i++) {
        printf("x[%3d] = %4.2f\t y[%3d] = %4.2f\n", i, x[i], i, y[i]);
    }  
    
    
    cudaMemcpy( dx, x, n*sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy( dy, y, n*sizeof(float), cudaMemcpyHostToDevice);
    
    dim3 block(64);
    dim3 grid( ( n + block.x - 1 ) / block.x );
    float alpha = 1.0;
    
    printf("\nlaunch kernel... ");
    cudaEventRecord(start);
    saxpy_kernel<<<grid, block >>>(n, alpha, dx, dy); // kernel computing y = y + alpha*x
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    printf("done.\n");
    
    float milliseconds = 0;
    cudaEventElapsedTime(&milliseconds, start, stop);

    printf("runtime [s]: %f  Performance [GFLOPs]: %.4f\n", milliseconds/1000.0,  
        (2*n)/(milliseconds/1000.0)*1e-9);
    
    
    cudaMemcpy( x, dy, n*sizeof(float), cudaMemcpyDeviceToHost);
    cudaMemcpy( y, dy, n*sizeof(float), cudaMemcpyDeviceToHost);
    

    printf("\nafterwards:\n");
    
    for (int i=0; i<10; i++) {
        printf("x[%3d] = %4.2f\t y[%3d] = %4.2f\n", i, x[i], i, y[i]);
    }  
    
    cudaFree(dx); // free the memory reserved for x and y
    cudaFree(dy);
    free(x); // free the memory reserved for x and y
    free(y);
    printf("Computation completed.\n");
    return 0;

}